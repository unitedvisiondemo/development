<?php include 'header.php'; ?>
<link rel="stylesheet" href="<?= base_url(); ?>resources/css/site-menu.css">
<div id="wrapper" class="toggled">
    <?php include 'site-menu.php'; ?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">


            <h3>Teacher Form</h3>
            <hr />

            <?php if ($this->session->userdata("flash_msg") !== null) { ?>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Info!</strong> <?= $this->session->userdata("flash_msg"); ?> 
                </div>    
            <?php } ?>

            <form method="post" action="<?= base_url(); ?>index.php/dashboard/saveTeacher">

                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5 text-align-right"><label>Username</label></div>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="username" placeholder="Username"/>
                            <p class="helping-text">Enter Username</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5 text-align-right"><label>Subject</label></div>
                        <div class="col-md-7">
                            <select class="form-control" name="subject_id">
                                <option value="">Select Subject</option>
                                <option value="1">English</option>
                                <option value="2">Mathematics</option>
                                <option value="3">Physics</option>
                                <option value="4">Chemistry</option>
                                <option value="5">Biology</option>
                            </select>
                            <p class="helping-text">Select a subject</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5 text-align-right"><label>Password</label></div>
                        <div class="col-md-7">
                            <input type="password" class='form-control' name='password' placeholder="Password">
                            <p class="helping-text">Enter a password</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5 text-align-right"><label>Confirm-Password</label></div>
                        <div class="col-md-7">
                            <input type="password" class='form-control' name='confirm_password' placeholder="Confirm Password">
                            <p class="helping-text">Enter confirm password</p>
                        </div>
                    </div>

                </div>
                <hr />

                <div class="submit">
                    <input type="submit" class="btn btn-info" value="Save" />
                    <input type="reset" class="btn btn-danger" value="Cancel"/>
                </div>

            </form>

        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<!-- /#wrapper -->
<?php include 'footer.php'; ?>