<?php include 'header.php'; ?>
<link rel="stylesheet" href="<?= base_url(); ?>resources/css/site-menu.css">
<div id="wrapper" class="toggled">
    <?php include 'site-menu.php'; ?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <h1>Welcome To United Vision</h1>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<!-- /#wrapper -->
<?php include 'footer.php'; ?>