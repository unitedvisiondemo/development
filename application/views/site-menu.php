<!-- Sidebar -->
<div id="sidebar-wrapper" style="background: #0747A6;">
    <ul class="sidebar-nav custom-hover">
        <li class="sidebar-brand">
            <a href="#"><img src="<?= base_url(); ?>resources/images/logo.png" width="190px;"/></a>
        </li>
        <li>
            <a href="<?= base_url(); ?>index.php/dashboard/createTeacher"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Create Teacher</a>
        </li>
        <li>
            <a href="<?= base_url(); ?>index.php/dashboard/createStudent"><i class="fa fa-user-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Create Student</a>
        </li>
        <li>
            <a href="<?= base_url(); ?>index.php/dashboard/logout"><i class="fa fa-window-close-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Logout</a>
        </li>
    </ul>
</div>
<!-- /#sidebar-wrapper -->