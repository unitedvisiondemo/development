<link rel="stylesheet" href="<?= base_url(); ?>resources/css/signin.css">
<div class="container">
    <form class="form-signin" method="post" action="<?= base_url(); ?>index.php/login/auth">
        <?php if ($this->session->userdata("flash_msg") !== null) { ?>
            <div class="alert alert-danger" style="max-width: 400px;">
                <strong>Info!</strong> <?= $this->session->userdata("flash_msg"); ?>
            </div>
        <?php } ?>
        <h2 class="form-signin-heading" style="text-align: center; margin-bottom: 30px; font-weight: bold;">Please sign in</h2>
        <!--<img class="form-signin-heading" src="<?= base_url(); ?>resources/images/logo2.png" width="90%" height="100px"/>-->
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="username" id="inputEmail" name="username" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
</div> <!-- /container -->