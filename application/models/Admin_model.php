<?php

class Admin_model extends CI_Model {

    public function dbSelectRow($selection, $table, $where){
        $sql = "Select ".$selection." From ".$table ." Where ";
        if($where){
            $sql .= $where;
        }
        $result = $this->db->query($sql);
        return $result->row();
    }
    
    public function dbInsert($table, $data){
        $this->db->insert($table, $data);
        return true;
    }
}