<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        $this->load->view("header");
        $this->load->view('signin/signin');
        $this->load->view("footer");
    }
    
    public function auth(){
        $user = $this->input->post("username");
        $pwd = $this->input->post("password");
        
        $encrypt_pwd = md5($pwd);
        $data = $this->admin_model->dbSelectRow("*","users","username='$user' ");
        
        if($data->username == $user && $data->password == $encrypt_pwd){
            if($data->active == 1){
                $this->session->set_userdata("flash_msg",NULL);
                $this->session->set_userdata("user_date",$data);
                $this->dashboard();
            } else {
                $this->session->set_userdata("flash_msg","User de-active.");
                redirect('/','refresh');
            }
        } else {
            $this->session->set_userdata("flash_msg","Invalid username or password.");
            redirect('/','refresh');
        }
    }
    
    function dashboard(){
        $this->load->view("dashboard");
    }
}
