<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function index() {
        $this->load->view('dashboard');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect("/", "refresh");
    }

    public function createTeacher() {
        $this->load->view("add-teacher-form");
    }

    public function createStudent() {
        $this->load->view("add-student-form");
    }

    public function saveTeacher() {
        $name = $this->input->post("username");
        $pwd = $this->input->post("password");
        $c_pwd = $this->input->post("confirm_password");
        $subject_id = $this->input->post("subject_id");
        if ($pwd == $c_pwd) {
            $data = array("username" => 'thr' . $name, "first_name" => $name, "role" => "teacher", "subjects_id" => $subject_id, "email" => "", "password" => md5($pwd), "active" => 1);
            $res = $this->admin_model->dbInsert("users", $data);
            if ($res) {
                $this->session->set_userdata("flash_msg","New Teacher Create Successfully.");
                redirect("dashboard/createTeacher");
            } else {
                $this->session->set_userdata("flash_msg","User could not create.");
                redirect("dashboard/createTeacher");
            }
        } else {
            $this->session->set_userdata("flash_msg","Confirm password does not match.");
            redirect("dashboard/createTeacher");
        }
    }
    
    public function saveStudent() {
        $name = $this->input->post("username");
        $pwd = $this->input->post("password");
        $c_pwd = $this->input->post("confirm_password");
        $class_id = $this->input->post("class_id");
        if ($pwd == $c_pwd) {
            $data = array("username" => 'std' . $name, "first_name" => $name, "role" => "student", "class_id" => $class_id, "email" => "", "password" => md5($pwd), "active" => 1);
            $res = $this->admin_model->dbInsert("users", $data);
            if ($res) {
                $this->session->set_userdata("flash_msg","New Student Create Successfully.");
                redirect("dashboard/createStudent");
            } else {
                $this->session->set_userdata("flash_msg","Student could not create.");
                redirect("dashboard/createStudent");
            }
        } else {
            $this->session->set_userdata("flash_msg","Confirm password does not match.");
            redirect("dashboard/createStudent");
        }
    }

}
